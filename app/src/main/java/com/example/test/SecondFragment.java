package com.example.test;

//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SecondFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String strtext = getArguments().getString("edttext");
        Log.i("LOG_ARGUMENT", strtext);
        return inflater.inflate(R.layout.fragment_second, container, false);
    }
}
